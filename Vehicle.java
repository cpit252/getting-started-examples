package vehciles;

public class Vehicle {
  private String make;
  private String model;
  private int year;
  private int numWheels;
  private int mileage;
  private String plateNumber;


  public Vehicle(String make, String model, int year, int numWheels) {
    this.make = make;
    this.model = model;
    this.year = year;
    this.numWheels = numWheels;
    this.mileage = 0;
    this.plateNumber = "unknown";
  }

  // Getters
 
  public String getMake() {
    return this.make;
  }

  public String getModel() {
    return this.model;
  }

  public int getYear() {
    return this.year;
  }

  public int getNumWheels() {
    return this.numWheels;
  }

  public int getMileage() {
    return this.mileage;
  }

  public String getPlateNumber() {
    return this.plateNumber;
  }

  public void setMileage(int newMileage) {
    if (newMileage < this.mileage) {
      throw new IllegalArgumentException();
    }
    this.mileage = newMileage;
  }

  // Setters/Mutators

  public void setPlateNumber(String plate) {
    this.plateNumber = plate;
  }

  @Override
  public String toString() {
    String str = "Vehcile info:\nMake: " + this.make + "\nModel:" + this.model +
        "\nYear" + this.year + "\nNumber of wheels: " + this.numWheels + 
        "\nMileage: " + this.mileage +"\nPlate number: " + this.plateNumber;
        return str;
    }
}
