package vehciles;

public class Truck extends Vehicle {

private int axles;

public Truck(String make, String model, int year, int numWheels){
    super(make, model, year, numWheels);
    this.axles = this.getNumWheels() / 2;;
}

public int getNumAxles() {
    return this.axles;
}
@Override
public String toString(){
    return super.toString() + "\nNumber of axles: " + this.axles;
}
}