package vehciles;

public class Van extends Vehicle {

private boolean hasSlidingDoor;

public Van(String make, String model, int year, int numWheels, boolean hasSlidingDoor){
    super(make, model, year, numWheels);
    this.hasSlidingDoor = hasSlidingDoor;
}

public boolean getHasSlidingDoor() {
    return this.hasSlidingDoor;
}

public void setHasSlidingDoor(boolean hasSlidingDoor){
    this.hasSlidingDoor = hasSlidingDoor;
}

@Override
public String toString(){
    return super.toString() + "\nHas Sliding Door: " + this.hasSlidingDoor;
}

}